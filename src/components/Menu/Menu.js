import React, {Component, Fragment} from 'react';
import './menu.scss';
import ThirdTitle from "../ThirdTitle/ThirdTitle";
import Linker from "../Link/Link";
import CasualLink from "../CasualLink/CasualLink";


class Menu extends Component{

    render() {
        return(
            <Fragment>
                <nav>
                    <ThirdTitle content={'Menu'}/>
                    <Linker text={'Home'} route={'/home'}/>
                    <Linker text={'Vous'} route={'/vous'}/>
                    <Linker text={'Moi'} route={'/moi'}/>
                    <Linker text={'Nous'} route={'/nous'}/>
                    <Linker text={'Mes Travaux'} route={'/travaux'}/>

                    <div>
                        <ThirdTitle content={'contact'}/>
                    </div>
                    <CasualLink source={'https://www.linkedin.com/in/marco-segretario/'} text={'Linkedin'}/>
                    <CasualLink source={'https://github.com/w-mseg'} text={'Github'}/>
                    <CasualLink source={'https://gitlab.com/W-mseg'} text={'Gitlab'}/>

                    <p>tel : 0479 888/074</p>
                </nav>
            </Fragment>
        );
    }


}

export default Menu;