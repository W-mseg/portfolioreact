import React, { Fragment } from "react";
import './maintitle.scss';

const MainTitle=({content,children})=>{
    return(
        <Fragment>
            {content?<h1 className={'maintitle'}>{content}</h1>:null}
            {children?<p className={'mainparag'}>{children}</p>:null}
        </Fragment>
    )
};

export default MainTitle;