import React, {Fragment} from 'react';

const ThirdTitle = ({content}) => {
    return (
        <Fragment>
            {content?<h3 className={'thirdtitle'}>{content}</h3>:null}
        </Fragment>
    );
};

export default ThirdTitle;