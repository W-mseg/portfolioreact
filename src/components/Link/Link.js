import React, {Fragment} from 'react';
import './link.scss';
import {Link, NavLink} from "react-router-dom";

const Linker = ({route,text}) => {
    return (
        <Fragment>
            <NavLink
                to={route?route:'/'}
                 activeClassName={'activelink'}
                 className={'linking'}>
                {text?text:'default'}
            </NavLink>
        </Fragment>
    );
};

export default Linker;