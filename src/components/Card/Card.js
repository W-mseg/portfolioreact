import React, {Fragment, useState} from 'react';
import Image from "../Image/Image";
import './card.scss';
import Titleh5 from "../Titleh5/Titleh5";


const Card = ({content,alt,source,contenttitle,href}) => {
let [useDeploy,setDeploy] = useState('off');

const handleDeployEnter=()=>{
    setDeploy(useDeploy = 'on');
};

const handleDeployExit=()=>{
    setDeploy(useDeploy = 'off');
};

    return (
        <Fragment>
            <div onMouseEnter={handleDeployEnter} onMouseLeave={handleDeployExit} className={'artik'}>
                <Titleh5 content={contenttitle}/>
                <a href={href?href:'#'} target={'blank'}><Image alt={alt} source={source}/></a>
                <div className={'textcard'}>
                {useDeploy==='on'?<p>{content?content:'default'}</p>:null}
                </div>
                <div className={'deployer'}>
                {useDeploy==='off'?<p>Voir</p>:<p>cacher</p>}
                </div>
            </div>
        </Fragment>
    );
};

export default Card;