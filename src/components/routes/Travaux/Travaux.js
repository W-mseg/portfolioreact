import React, {Fragment} from 'react';
import './travaux.scss';
import Menu from "../../Menu/Menu";
import Card from "../../Card/Card";
import hackpoul from '../../../images/poulet.png';
import foodog from '../../../images/foodog.jpg';
import lph from '../../../images/restaurant-devanture.jpg';
import react from '../../../images/react.png';
import MainTitle from "../../MainTitle/MainTitle";
import 'animate.css/animate.css';


const Travaux = () => {
    return (
        <Fragment>
            <MainTitle content={'Mes travaux'}/>
            <br/>
            <div className={'wrapper containerartik'}>

                <Card
                    alt={'raspbery'}
                    href={'https://github.com/W-mseg/hackersPoulete'}
                    source={hackpoul} contenttitle={'Hackers poulette'}
                    content={'Mon role : Back-end & construction d\'API'}
                 />
                <Card
                    alt={'dog'}
                    href={'https://github.com/W-mseg/T7-FooDog-WP'}
                    source={foodog}
                    contenttitle={'FooDog'}
                    content={'Mon role : Creation et optimisation des boucles'}
                />
                <Card
                    alt={'poulet'}
                    href={'https://github.com/W-mseg/restaurant-css-framework'}
                    source={lph}
                    contenttitle={'Los Pollos Hermanos'}
                    content={'Mon role : Creation de la carte menu'}
                />
                <Card
                    alt={'react'}
                    href={'https://gitlab.com/W-mseg/reactlivecoding-becode'}
                    source={react}
                    contenttitle={'Resumés live coding'}
                    content={'Résumés des live coding fait à BeCode'}
                />
            </div>
            <Menu/>
        </Fragment>
    );
};

export default Travaux;