import React, {Fragment} from 'react';
import {BrowserRouter,Route,Switch} from "react-router-dom";
import './style.scss';
import 'animate.css/animate.min.css';
import MainTitle from "../../MainTitle/MainTitle";
import ThirdTitle from "../../ThirdTitle/ThirdTitle";
import Menu from "../../Menu/Menu";
import 'animate.css/animate.css';


const MainApp = () => {
    return (
        <Fragment>
                <div className={'wrapper'}>
                    <MainTitle content={'Marco Segretario'}>
                        Developpeur web junior
                    </MainTitle>
                    <ThirdTitle content={'Interessé par : stage'}/>
                    <br/>
                    <div className={'divinfo'}>

                    <p>Un portfolio fait avec :</p>
                    <ul>
                        <li>ReactJS</li>
                        <li>ReactRouter</li>
                        <li>Css grid / flexbox</li>
                        <li>Les dernieres fonctionnalités ES6</li>
                        <li>ReactHooks</li>
                        <li>Animate.css</li>
                    </ul>
                    <p>naviguez via le menu à gauche</p>
                    </div>
                </div>
                <Menu/>

        </Fragment>

    );
};

export default MainApp;