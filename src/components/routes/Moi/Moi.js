import React, {Fragment} from 'react';
import Menu from "../../Menu/Menu";
import Image from "../../Image/Image";
import whoami from './img/whoami.png';
import informatique from './img/informatique.jpg';
import react from '../../../images/react.png';
import smile from './img/smile.jpg';
import socrate from './img/socrate.png';
import 'animate.css/animate.css';


const Moi = () => {
    return (
        <Fragment>
            <div className={'wrapper appformat'}>
            <h1>Moi</h1>
               <h3>Qui suis je ?</h3>
                <Image source={whoami} alt={'quisuisje'}/>
                <p>Je suis Marco Segretario, 26 ans, un passionné d'informatique depuis mon plus jeune âge</p>
                <Image source={informatique} alt={'informatique'}/>
                <p>Je suis très fan de la technique ReactJS et NodeJS et cherche à me spécialiser à travers des projets
                    concrets lors d'un stage</p>
                <Image source={react} alt={'react'}/>
                <p>Un argument qui vous fera pencher en ma faveur ce n'est pas mes compétences déjà suffisantes ou encore
                    ma manière de communiquer mais ma bonne humeur ! Car mon sourire n'est pas une façade je saurais rendre
                    de bonne humeur mes collègues !</p>
                <Image source={smile} alt={'sourrire'}/>
                <p>J'ai des projets qui nécessitent du temps et des connaissances c'est ce que je recherche afin de
                    pouvoir enseigner légitimement aux gens les connaissances que j'auraient acquises.</p>
                <Image source={socrate} alt={'socrate'}/>

            </div>
            <Menu/>
        </Fragment>
    );
};

export default Moi;