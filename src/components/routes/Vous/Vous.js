import React, {Fragment} from 'react';
import Menu from "../../Menu/Menu";
import Image from "../../Image/Image";
import './vous.scss';
import value from './img/values.png';
import hierarchie from './img/hierarchie.png';
import human from './img/human.png';
import eco from './img/ecologie.jpg';
import 'animate.css/animate.css';



const Vous = () => {
    return (
        <Fragment>
            <Menu/>
            <div className={'wrapper appformat'}>

                <h1>Vous</h1>
                <br/>
                <p>Vous êtes une entreprise pour qui je partage les valeurs et pour cette raison je souhaite faire partie
                de votre équipe !</p>
                <Image source={value} alt={'valeurs'}/>


                <p>Je m'interesse au côté humain des entreprises, y a-t-il un décalage entre les modules hiérarchiques ?</p>
                <Image source={hierarchie} alt={'hierarchie'}/>

                <p>Sommes-nous considérés comme des humains ?</p>
                <Image source={human} alt={'humancode'}/>

                <p>Et Quid de l'écologie ??</p>
                <Image source={eco} alt={'ecologie'}/>

                <p>À mon sens une entreprise qui respecte ce genre de "détail" auras toute mon attention et ce qu'el que
                    soit la technique utilisée chez vous !</p>
            </div>
        </Fragment>
    );
};

export default Vous;