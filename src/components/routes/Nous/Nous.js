import React, {Fragment} from 'react';
import Menu from "../../Menu/Menu";
import MainTitle from "../../MainTitle/MainTitle";
import Image from "../../Image/Image";
import fusion from './img/fusion.jpg';
import cafe from './img/cafe.jpg';
import feuglace from './img/feuglace.jpg';
import team from './img/team.png';
import 'animate.css/animate.css';


const Nous = () => {
    return (
        <Fragment>
            <div className={'wrapper appformat'} >

            <MainTitle content={'nous'}/>
            <h3>On fusionne ?</h3>
                <Image source={fusion} alt={'fusion'}/>
                <p>Je suis intéressé par vous rejoindre et sans nul doute que je peux vous convenir ! Mes seules exigences
                    sont d'apprendre ET la possibilité d'apporter mon café au lait. (Une affaire en Or )</p>
                <Image source={cafe} alt={'café'}/>
                <p>La passion brule en moi tellement fort que je ferais fondre la glace entre moi et mes nouveaux collègues</p>
                <Image source={feuglace} alt={'feuglace'}/>
                <p>Je suis donc aussi humain qu'une machine de productivité alors quoi de mieux que de faire équipe ?</p>
                <Image source={team} alt={'equipe'}/>
            </div>
            <Menu/>
        </Fragment>
    );
};

export default Nous;