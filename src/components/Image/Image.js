import React, {Fragment, useState} from 'react';
import './image.scss';



const Image = ({source,alt}) => {

    let [useClasse,SetClasse] = useState('');


    const handleHover=()=>{

        let random = Math.floor(Math.random() * Math.floor(3));
        switch (random) {
            case 0:
                SetClasse(useClasse = 'bounce ');
                break;
            case 1:
                SetClasse(useClasse = 'pulse ');
                break;
            case 2:
                SetClasse(useClasse = 'rubberBand ');
                break;
            default:
                SetClasse(useClasse = 'bounce ');
                break;
        }
    };

    return (
        <Fragment>
            <img onMouseOver={handleHover} className={useClasse+'imageformat'} src={source?source:'https://picsum.photos/280/150'} alt={alt?alt:'image'}/>
        </Fragment>
    );
};

export default Image;