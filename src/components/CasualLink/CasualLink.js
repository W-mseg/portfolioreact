import React, {Fragment} from 'react';

const CasualLink = ({text,source}) => {
    return (
        <Fragment>
            <a className={'linking'} href={source?source:'#'} target={'blank'}>{text?text:'default'}</a>
        </Fragment>
    );
};

export default CasualLink;