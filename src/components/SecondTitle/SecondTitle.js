import React, {Fragment} from 'react';

const SecondTitle = ({content}) => {
    return (
        <Fragment>
            {content?<h2>{content}</h2>:null}
        </Fragment>
    );
};

export default SecondTitle;