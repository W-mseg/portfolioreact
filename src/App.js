import React, { Fragment } from "react";
import {BrowserRouter,Route,Switch,Redirect} from "react-router-dom";
import 'animate.css/animate.min.css';
import MainApp from "./components/routes/MainApp/MainApp";
import Vous from "./components/routes/Vous/Vous";
import Moi from "./components/routes/Moi/Moi";
import Nous from "./components/routes/Nous/Nous";
import Travaux from "./components/routes/Travaux/Travaux";
import Linkedin from "./components/routes/Linkedin/Linkedin";
import Github from "./components/routes/Github/Github";
import Gitlab from "./components/routes/Gitlab/Gitlab";


const App=()=>{
  return(
    <div>
        <Switch>
            <Route exact path="/vous">
                <div className={'application'}>
                    <Vous/>
                </div>
            </Route>

            <Route exact path={"/moi"}>
                <div className={'application'}>
                    <Moi/>
                </div>
            </Route>

            <Route exact path={"/nous"}>
                <div className={'application'}>
                    <Nous/>
                </div>
            </Route>

            <Route exact path={"/travaux"}>
                <div className={'application'}>
                    <Travaux/>
                </div>
            </Route>

            <Route exact path={"/linkedin"}>
                <div className={'application'}>
                    <Linkedin/>
                </div>
            </Route>

            <Route exact path={"/github"}>
                <div className={'application'}>
                    <Github/>
                </div>
            </Route>

            <Route exact path={"/gitlab"}>
                <div className={'application'}>
                    <Gitlab/>
                </div>
            </Route>

            <Route exact path={'/home'}>
                <div className={'application'}>
                    <MainApp/>
                </div>
            </Route>
            <Route path="/">
                <div className={'application'}>
                <Redirect to={'/home'}/>
                </div>
            </Route>
        </Switch>
    </div>
  )
};

export default App;